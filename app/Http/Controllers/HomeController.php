<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use TCG\Voyager\Models\Post;
use Validator;

class HomeController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function webpage(Request $request)
    {
        //categories
//        $categoryIds = Category::all()->pluck('id');
//
//        $categories = array();
//
//        foreach ($categoryIds as $categoryId)
//        {
//            $categoryName = Category::where('id', $categoryId)->first()->name;
//
//            $count = Post::where('category_id', $categoryId)->count();
//
//            $categories[$categoryName] = $count;
//        }

        $categories = Category::all();
        $posts = Post::all();

        //authors
        $authorObj = Author::all();

        $author = array();
        foreach ($authorObj as $item)
        {
            $author[$item->name] = $item->intro;
        }

        return view('welcome', compact(['categories', 'author', 'posts']));
    }

    public function signUpPage (Request $request)
    {
        return view('signup');
    }

    public function login (Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'signin_email' => 'required|email',
//            'signin_password' => 'required|alphaNum|min:8',
//        ]);
//
//        if ($validator->fails())
//        {
//            return Redirect::to('http://fyp.local/signin')->withErrors($validator);
//        }
//        else{
//            return Redirect::to('http://fyp.local/');
//        }
        return Redirect::to('http://fyp.local/');

    }



}


<!-- jQuery -->
<script src="{{url('/')}}/assets/js/jquery-2.1.0.min.js"></script>

<!-- Bootstrap -->
<script src="{{url('/')}}/assets/js/popper.js"></script>
<script src="{{url('/')}}/assets/js/bootstrap.min.js"></script>

<!-- Plugins -->
<script src="{{url('/')}}/assets/js/scrollreveal.min.js"></script>
<script src="{{url('/')}}/assets/js/waypoints.min.js"></script>
<script src="{{url('/')}}/assets/js/jquery.counterup.min.js"></script>
<script src="{{url('/')}}/assets/js/imgfix.min.js"></script>
<script src="{{url('/')}}/assets/js/mixitup.js"></script>
<script src="{{url('/')}}/assets/js/accordions.js"></script>

<!-- Global Init -->
<script src="{{url('/')}}/assets/js/custom.js"></script>

<script src="{{url('/')}}/assets/vendor/jquery/jquery.min.js"></script>
<script src="{{url('/')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{url('/')}}/assets/js/owl-carousel.js"></script>
<script src="{{url('/')}}/assets/js/animation.js"></script>
<script src="{{url('/')}}/assets/js/imagesloaded.js"></script>
<script src="{{url('/')}}/assets/js/templatemo-custom.js"></script>

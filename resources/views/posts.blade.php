<!DOCTYPE html>
<html lang="en">
<head>

@include('head')

    <!-- load stylesheets -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">  <!-- Google web font "Open Sans" -->
    <link rel="stylesheet" href="{{url('/')}}/assets/posts/font-awesome-4.7.0/css/font-awesome.min.css">                <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/posts/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/posts/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/posts/css/datepicker.css"/>

</head>

<style>
    .tm-pb-4 {
        padding-bottom: 100px;
    }
    .tm-pt-5 {
        padding-top: 150px;
    }
    .text-center {
        text-align: center!important;
    }
    .tm-article {
        padding: 40px;
        transition: all 0.3s ease;
    }
    .tm-color-primary {
        color: #ed563b;
    }
    .tm-margin-b-20 {
        margin-bottom: 20px;
    }
    .tm-fa-6x {
        font-size: 6em;
    }
    .tm-article-title-1 {
        font-size: 1.3rem;
        font-weight: 600;
        margin-bottom: 20px;
    }
    .tm-color-primary {
        color: #ed563b;
    }
    p {
        color: #898989;
        line-height: 1.9;
    }
    .tm-font-semibold {
        font-weight: 600;
    }
    .tm-color-primary {
        color: #ed563b;
    }
    .tm-article:hover {
        -webkit-box-shadow: 0px 0px 7px 0px rgb(214 214 214);
        -moz-box-shadow: 0px 0px 7px 0px rgba(214,214,214,1);
        box-shadow: 0px 0px 7px 0px rgb(214 214 214);
        transform: scale(1.1);
    }

</style>

<body>

@include('posts-header')

@include('posts-videobanner')

<style>

</style>

<div class="tm-main-content" id="top">
    <div class="tm-section tm-position-relative">
        <div class="container tm-pt-5 tm-pb-4">
            <div class="row text-center">
                @foreach($posts as $post)
                    <article class="col-sm-12 col-md-4 col-lg-4 col-xl-4 tm-article">
{{--                        <i class="fa tm-fa-6x fa-legal tm-color-primary tm-margin-b-20"></i>--}}
                        <h3 class="tm-color-primary tm-article-title-1">{{ $post->title }}</h3>
                        <div style="overflow: hidden"><p>{{ $post->body }}</p></div>
                        <br>
                        <a href="{{ route('post', $post->id) }}" class="text-uppercase tm-color-primary tm-font-semibold">Continue reading...</a>
                    </article>
                @endforeach
            </div>
        </div>
    </div>
</div>

@include('footer')

</body>
</html>








<!-- ***** Call to Action Start ***** -->
<section class="section" id="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="cta-content">
                    <h2>are you an <em>author</em>?</h2>
                    <p style="color: #ed563b">Send an email on the following to register yourself as an author!</p>
                    <div class="main-button scroll-to-section">
                        <a href="#our-classes" style="text-transform: lowercase;">random@email.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Call to Action End ***** -->

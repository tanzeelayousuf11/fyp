{{--<!-- ***** Contact Us Area Starts ***** -->--}}
{{--<section class="section" id="contact-us">--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-6 col-md-6 col-xs-12">--}}
{{--                <div id="map">--}}
{{--                    <iframe src="https://maps.google.com/maps?q=Av.+L%C3%BAcio+Costa,+Rio+de+Janeiro+-+RJ,+Brazil&t=&z=13&ie=UTF8&iwloc=&output=embed" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-6 col-md-6 col-xs-12">--}}
{{--                <div class="contact-form">--}}
{{--                    <form id="contact" action="#" method="post">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-6 col-sm-12">--}}
{{--                                <fieldset>--}}
{{--                                    <input name="name" type="text" id="name" placeholder="Your Name*" required="">--}}
{{--                                </fieldset>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-6 col-sm-12">--}}
{{--                                <fieldset>--}}
{{--                                    <input name="email" type="text" id="email" pattern="[^ @]*@[^ @]*" placeholder="Your Email*" required="">--}}
{{--                                </fieldset>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-12 col-sm-12">--}}
{{--                                <fieldset>--}}
{{--                                    <input name="subject" type="text" id="subject" placeholder="Subject">--}}
{{--                                </fieldset>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <fieldset>--}}
{{--                                    <textarea name="message" rows="6" id="message" placeholder="Message" required=""></textarea>--}}
{{--                                </fieldset>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-12">--}}
{{--                                <fieldset>--}}
{{--                                    <button type="submit" id="form-submit" class="main-button">Send Message</button>--}}
{{--                                </fieldset>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- ***** Contact Us Area Ends ***** -->--}}

<style>

    .contact-us-bg{
        background-color: #ed563b;
        margin-top: 100px;
        margin-bottom: 50px;
    }
    .contact-us-body{
        padding-top: 70px;
        padding-bottom: 70px;
        color: white;
        text-align: center;
        font-size: large;
    }

</style>

<div class="container-fluid contact-us-bg" id="contact-us">
    <div class="contact-us-body">if you have any queries, contact us at random@email.com</div>
</div>

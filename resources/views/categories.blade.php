<style>
    .categories-icon{
        background-color: #ed563b;
        width: 100px;
        float: left;
        margin-right: 30px;
        height: 90px;
        color: white;
    }
    a {
        text-decoration: none;
    }
</style>

<!-- ***** Features Item Start ***** -->
<section class="section" id="our-categories">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading" style="margin-top: 100px">
                    <h2>our <em>categories</em></h2>
                    <img src="assets/images/line-dec.png" alt="waves">
                </div>
            </div>
        </div>
        <div class="container-fluid row">
            @foreach($categories as $category)
                <?php
                    $count = $posts->where('category_id', $category->id)->count();
                ?>
                <div class="col-md-6 col-xs-12">
                    <ul class="features-items">
                        <li class="feature-item">
                            <div class="left-icon">
                                <h1 class="categories-icon" style="padding-left: 40px; padding-top: 20px;">{{ $category->name[0] }}</h1>
{{--                                <img src="assets/images/features-first-icon.png" alt="First One">--}}
                            </div>
                            <div class="right-content">
                                <h4 style="padding-top: 15px">{{ $category->name }}</h4>
{{--                                <p>Please do not re-distribute this template ZIP file on any template collection website. This is not allowed.</p>--}}
                                @if ($count > 0)
                                    <p>This category has {{ $count }} post(s) right now.<a href="{{route('posts', [$category->id])}}"> Click here to view posts in this category.</a></p>
                                @else
                                    <p>This category has 0 post(s) right now for you. Register as an author and add posts.</p>
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            @endforeach


        </div>
    </div>
</section>
<!-- ***** Features Item End ***** -->

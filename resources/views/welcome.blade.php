<!DOCTYPE html>
<html lang="en">

<head>

@include('head')

</head>

<body>

@include('header')

@include('videobanner')

@include('categories')

@include('banner')

@include('authors')

@include('contactus')

@include('footer')

@include('js')

</body>
</html>







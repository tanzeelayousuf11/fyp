
<!-- ***** Main Banner Area Start ***** -->
<div class="main-banner" id="top">
    <video autoplay muted loop id="bg-video">
        <source src="{{url('/')}}/assets/images/banner_pages.mp4" type="video/mp4" />
    </video>

    <div class="video-overlay header-text">
        <div class="caption">
            <h2><em>POSTS</em> {{ $categoryName }}</h2>
        </div>
    </div>
</div>
<!-- ***** Main Banner Area End ***** -->

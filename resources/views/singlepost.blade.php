<!DOCTYPE html>
<html>
<head>
    @include('head')
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<style>
    .singlepost-body
    {
        margin: 100px;
        overflow-wrap: anywhere;
        color: #6f6868;
    }
</style>

<body>

@include('header')

@include('singlepost-videobanner')

<div class="singlepost-body">
    {{ $post->body }}
</div>

@include('footer')

@include('js')

</body>
</html>


<!DOCTYPE html>
<html lang="en">

<head>

@include('head')
    <link rel="stylesheet" href="{{url('/')}}/assets/css/signup.css">
</head>

<style>
    body{
        background-color: #211f1f;
    }
    .img p{
        color: #ffffff;
    }
    button:focus{
        outline: none;
    }
</style>

<body>

@include('header')

@include('banner')

<div class="cont">
    <div class="form sign-in">
        <h2>Welcome back</h2>
        <form action="{{route('loginForm')}}">
            <label style="margin-top: 80px">
                <span>Email</span>
                <input type="email" name="signin_email" required />
            </label>
            <label>
                <span>Password</span>
                <input type="password" name="signin_assword" required />
            </label>
            <button type="submit" class="submit">Login</button>
        </form>
    </div>
    <div class="sub-cont">
        <div class="img">
            <div class="img__text m--up">
                <h2>New here?</h2>
                <p>Sign up and discover great amount of new opportunities!</p>
            </div>
            <div class="img__text m--in">
                <h2>One of us?</h2>
                <p>If you already has an account, just sign in. We've missed you!</p>
            </div>
            <div class="img__btn">
                <span class="m--up">Sign Up</span>
                <span class="m--in">Login</span>
            </div>
        </div>
        <div class="form sign-up">
            <h2>Time to feel like home</h2>
            <form action="{{route('loginForm')}}">
                <label>
                    <span>Name</span>
                    <input type="text" name="name" required />
                </label>
                <label>
                    <span>Email</span>
                    <input type="email" name="signup_email" required/>
                </label>
                <label>
                    <span>Password</span>
                    <input type="password" name="signup_password" required/>
                </label>
                <label>
                    <span>Contact Number</span>
                    <small>(with country code)</small>
                    <input type="text" name="phone" required/>
                </label>
                <button type="submit" class="submit">Sign Up</button>
            </form>
        </div>
    </div>
</div>


@include('contactus')

@include('footer')

@include('js')
<script src="{{url('/')}}/assets/js/signup.js"></script>

</body>
</html>


<!-- ***** Main Banner Area Start ***** -->
<div class="main-banner" id="top">
    <video autoplay muted loop id="bg-video">
        <source src="{{url('/')}}/assets/images/banner_pages.mp4" type="video/mp4" />
    </video>

    <div class="video-overlay header-text">
        <div class="caption">
            <h6>knowledge is power</h6>
            <h2>get powerful with our <em>magazine</em></h2>
            <div class="main-button scroll-to-section">
                <a href="{{url('/')}}/signin">Become a member</a>
            </div>
        </div>
    </div>
</div>
<!-- ***** Main Banner Area End ***** -->


<!-- ***** Testimonials Starts ***** -->
<section class="section" id="authors">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading">
                    <h2>our <em>authors</em></h2>
                    <img src="{{url('/')}}/assets/images/line-dec.png" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($author as $name => $intro)
                <div class="col-lg-4">
                    <div class="trainer-item" style="text-align: center;">
                        <div class="image-thumb">
                            <img src="{{url('/')}}/assets/images/first-trainer.jpg" alt="">
                        </div>
                        <div class="down-content" style="margin-top: 20px">
                            <span style="color: #ed563b">Author at our Magazine</span>
                            <h4>{{ $name }}</h4>
                            <p>{{ $intro }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- ***** Testimonials Ends ***** -->
